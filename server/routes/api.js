const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/')

const BlogPost = require('../models/blogpost.model.ts')

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('Blog Posts API Working')
})

// Get all posts
router.get('/posts', (req, res) => {
  BlogPost.find({}).sort('-created_at').exec((err, posts) => {
    if (err) res.status(500).send(err)
    res.status(200).json(posts)
  })
})

// Post new blog post
router.post('/posts', (req, res) => {
  let newPost = BlogPost({
    title: req.body.title,
    content: req.body.content,
    tags: req.body.tags,
    created_at: new Date()
  })

  newPost.save((err) => {
    if (err) res.status(500).send(err)
    res.status(200).json(newPost)
  })
})

// Get single post
router.get('/posts/:id', (req, res) => {
  BlogPost.findById(req.params.id, (err, post) => {
    if (err) res.status(500).send(err)
    res.status(200).json(post)
  })
})

// Get all posts with a particular tag
router.get('/posts/filter/:filter', (req, res) => {
  BlogPost.find({ tags: req.params.filter }).sort('-created_at').exec((err, posts) => {
    if (err) res.status(500).send(err)
    res.status(200).json(posts)
  })
})

module.exports = router
