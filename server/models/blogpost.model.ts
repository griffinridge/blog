const mongoose = require('mongoose')
const Schema = mongoose.Schema

let blogPostSchema = new Schema({
  title: String,
  content: String,
  tags: Array,
  created_at: Date
});

let BlogPost = mongoose.model('BlogPost', blogPostSchema);

module.exports = BlogPost;
