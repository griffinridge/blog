import { Component } from '@angular/core'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { MockBackend } from '@angular/http/testing'
import { Http, BaseRequestOptions } from '@angular/http'
import { FormsModule } from '@angular/forms'

import { CreatePostComponent } from '../CreatePost/createpost.component'
import { PostsListComponent } from './postslist.component'
import { PostComponent } from '../Post/post.component'

import { PostsService } from '../../services/Posts/posts.service'

describe('PostsListComponent', () => {
  let component: PostsListComponent
  let fixture: ComponentFixture<PostsListComponent>

  const router = { navigate: jasmine.createSpy('navigate') }
  const mockPosts: Array<Object> = [
    { 'foo': 'FOO' },
    { 'bar': 'BAR' },
    { 'baz': 'BAZ' }
  ]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreatePostComponent,
        PostsListComponent,
        PostComponent,
      ],
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      providers: [
        PostsService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, mockPosts) => {
            return new Http(mockBackend, mockPosts)
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })

    TestBed.overrideComponent(PostComponent, {
      set: { template: '<div>PostComponent</div>' }
    })
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges()
  })

  it('should be a PostListComponent', () => {
    expect(component instanceof PostsListComponent).toBeTruthy()
  })

  describe('properties', () => {
    it('should set postId to be an empty string', () => {
      expect(component.postId).toBe('')
    })

    it('should set filter to be an empty string', () => {
      expect(component.filter).toBe('')
    })

    it('should set posts to be an empty Array', () => {
      expect(component.posts instanceof Array).toBe(true)
      expect(component.posts.length).toBe(0)
    })

    it('should set newPost to be an empty post object', () => {
      expect(component.newPost.title).toBe('')
      expect(component.newPost.content).toBe('')
      expect(component.newPost.tagsString).toBe('')
    })

    it('should set singleView to be false when postId does NOT exist', () => {
      component.postId = undefined
      expect(component.singleView).toBe(false)
    })
  })

  describe('fetchPosts', () => {
    describe('when postId exists', () => {
      beforeEach(() => {
        spyOn(component, 'getPost')
        component.postId = 'foo'
        component.fetchPosts()
      })

      it('should set singleView to true', () => {
        expect(component.singleView).toBe(true)
      })

      it('should call getPost', () => {
        expect(component.getPost).toHaveBeenCalled()
      })
    })

    describe('when postId does NOT exist', () => {
      beforeEach(() => {
        spyOn(component, 'getPost')
        component.postId = undefined
        component.fetchPosts()
      })

      it('should NOT set singleView to true', () => {
        expect(component.singleView).not.toBe(true)
      })

      it('should not call getPost', () => {
        expect(component.getPost).not.toHaveBeenCalled()
      })

      describe('when postId does NOT exist and filter exists', () => {
        beforeEach(() => {
          spyOn(component, 'getFilteredPosts')
          component.postId = undefined
          component.filter = 'foobarbaz'
          component.fetchPosts()
        })

        it('should call getFilteredPost with the filter', () => {
          expect(component.getFilteredPosts).toHaveBeenCalledWith('foobarbaz')
        })
      })

      describe('when postId does NOT exist and filter does NOT exists', () => {
        beforeEach(() => {
          spyOn(component, 'getAllPosts')
          component.postId = undefined
          component.filter = undefined
          component.fetchPosts()
        })

        it('should call getAllPost with the filter', () => {
          expect(component.getAllPosts).toHaveBeenCalledWith()
        })
      })
    })
  })
})
