import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { Subscription } from 'rxjs/Subscription'

import { PostsService } from '../../services/Posts/posts.service'

@Component({
  selector: 'app-posts-list',
  templateUrl: './postslist.component.html',
  styleUrls: ['./postslist.component.scss']
})

export class PostsListComponent implements OnInit {
  /**
   * @property postId
   * The post ID if provided
   */
  public postId = ''

  /**
   * @property filter
   * Tag filter to filter posts results
   */
  public filter = ''

  /**
   * @property posts
   * An array of blog  posts
   */
  public posts: Array<any> = []

 /**
   * @property newPost
   * A new blank post
   */
  public newPost: any = {
    'title': '',
    'content': '',
    'tagsString': ''
  }

  /**
   * @property singleView
   * @default false
   *
   * Whether we are viewing a single post
   */
  public singleView = !!this.postId

  /**
   * @property subscription
   * Subcription used to observe changes to the filter
   */
  private subscription: Subscription

  constructor (private postsService: PostsService,
               private router: Router,
               private activeRoute: ActivatedRoute) { }

  ngOnInit (): void {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        this.filter = this.activeRoute.snapshot.params['filter']
        this.postId = this.activeRoute.snapshot.params['id']

        this.fetchPosts()
      }
    })
  }

  /**
   * @method fetchPosts
   * Fetches the appropriate posts
   */
  fetchPosts () {
    if (this.postId) {
      this.singleView = true
      this.getPost()
    } else if (this.filter) {
      this.getFilteredPosts(this.filter)
    } else {
      this.getAllPosts()
    }
  }

  /**
   * @method clearNewPost
   * Clears the new post form model
   */
  clearNewPost () {
    for (const key in this.newPost) {
      if (this.newPost.hasOwnProperty(key)) {
        this.newPost[key] = ''
      }
    }
  }

  /**
   * @method identifyPost
   * @param index Number
   * @param post Object
   *
   * Identifies the post by id
   */
  identifyPost (index, post): Number {
    return post._id
  }

  /**
   * @method getAllPosts
   *
   * Retrieves all blog posts
   */
  getAllPosts (): void {
    this.postsService.getAllPosts().subscribe(posts => this.posts = posts)
  }

  /**
   * @method getFilteredPosts
   *
   * Retrieves all blog posts with the specified tag
   */
  getFilteredPosts (filter): void {
    this.postsService.getFilteredPosts(filter).subscribe(posts => this.posts = posts)
  }

  /**
   * @method getPost
   *
   * Retrieves a single blog post
   */
  getPost (): void {
    this.postsService.getPost(this.postId).subscribe(post => this.posts = [post])
  }

  /**
   * @method openPost
   * @param postId: Number
   *
   * Opens a single post
   */
  openPost (postId): void {
    this.router.navigate(['/posts', postId])
  }

  /**
   * @method createPost
   *
   * Creates a new post with the current form data
   */
  createPost (): void {
    this.postsService.createPost(this.newPost).subscribe((post) => {
      this.clearNewPost()
      this.fetchPosts()
    })
  }
}
