import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-post-tags',
  templateUrl: './posttags.component.html',
  styleUrls: ['./posttags.component.scss']
})

export class PostTagsComponent {
  @Input() tags: Array<String>

  constructor (private router: Router) {}

  filterTag (tag) {
    this.router.navigate(['/posts/filter', tag])
  }
}
