import { Router } from '@angular/router'
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule } from '@angular/forms'

import { PostTagsComponent } from './posttags.component'


describe('PostTagsComponent', () => {
  let component: PostTagsComponent
  let fixture: ComponentFixture<PostTagsComponent>

  const router = { navigate: jasmine.createSpy('navigate') }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostTagsComponent
      ],
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: Router, useValue: router }
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(PostTagsComponent);
      component = fixture.componentInstance;
    })
  }))

  it('should be a PostTagsComponent', () => {
    expect(component instanceof PostTagsComponent).toBeTruthy()
  })

  describe('filterTag', () => {
    it('should go to the filter URL with the provided tag', () => {
      component.filterTag('foo')

      expect(router.navigate).toHaveBeenCalledWith(['/posts/filter', 'foo'])
    })
  })
})
