import { Component, Input, Output, EventEmitter } from '@angular/core'
import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-create-post',
  templateUrl: './createpost.component.html',
  styleUrls: ['./createpost.component.scss']
})

export class CreatePostComponent {
  @Input() newPost: any
  @Output() postSubmit: EventEmitter<Object> = new EventEmitter()

  public showForm: Boolean = false

  createPost () {
    this.newPost.tags = this.processTagsString()
    this.postSubmit.emit(this.newPost)
    this.showForm = false;
  }

  toggleForm () {
    this.showForm = !this.showForm
  }

  processTagsString () {
    if (this.newPost.tagsString.trim().length === 0) { return [] }

    return this.newPost.tagsString.split(',').map((tag) => {
      return tag.trim()
    })
  }
}
