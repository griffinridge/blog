import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule } from '@angular/forms'

import { CreatePostComponent } from './createpost.component'

describe('CreatePostComponent', () => {
  let component: CreatePostComponent,
      fixture: ComponentFixture<CreatePostComponent>,
        fakeTags: Array<String>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreatePostComponent
      ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fakeTags = [ 'foo', 'bar', 'baz' ]
    fixture = TestBed.createComponent(CreatePostComponent)
    component = fixture.componentInstance
    component.newPost = {
    'title': 'Content',
      'conten': 'Content',
      'tags': fakeTags
    }
    fixture.detectChanges()
  });

  it('should be a CreatePostComponent', () => {
    expect(component instanceof CreatePostComponent).toBeTruthy()
  })

  describe('createPost', () => {

    beforeEach(() => {
      spyOn(component, 'processTagsString').and.returnValue(fakeTags)
      spyOn(component.postSubmit, 'emit')
      component.createPost()
    })

    it('should set newPost.tags to be the result of processTagsString', () => {
      expect(component.newPost.tags).toBe(fakeTags)
    })

    it('should set newPost.tags to be the result of processTagsString', () => {
      expect(component.postSubmit.emit).toHaveBeenCalledWith(component.newPost)
    })

    it('should set showForm to default as false', () => {
      expect(component.showForm).toBe(false)
    })
  })

  describe('showForm', () => {
    it('should set showForm to false by default', () => {
      expect(component.showForm).toBe(false)
    })
  })

  describe('toggleForm', () => {
    it('should toggle showForm froom true to false', () => {
      component.showForm = false
      component.toggleForm()
      expect(component.showForm).toBe(true)
    })

    it('should toggle showForm from false to true', () => {
      component.showForm = true
      component.toggleForm()
      expect(component.showForm).toBe(false)

    })
  })

  describe('processTagsString', () => {
    it('should return an empty array when there is no tagsString data', () => {
      component.newPost.tagsString = ''
      expect(component.processTagsString() instanceof Array).toBe(true)
      expect(component.processTagsString().length).toBe(0)
    })

    it('should return an array or trimmed strings from a comma separated list', () => {
      component.newPost.tagsString = 'foo, bar , baz,   qux.quux'
      const processed = component.processTagsString()

      expect(processed[0]).toBe('foo')
      expect(processed[1]).toBe('bar')
      expect(processed[2]).toBe('baz')
      expect(processed[3]).toBe('qux.quux')
    })
  })
})
