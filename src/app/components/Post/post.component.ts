import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-blog-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class PostComponent {
  @Input() post: any
  @Input() singleView: Boolean
  @Output() openPost: EventEmitter<Object> = new EventEmitter()

  onTitleClick () {
    this.openPost.emit(this.post._id)
  }
}
