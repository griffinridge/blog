import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'

import { MomentModule } from 'angular2-moment'

import { PostComponent } from './post.component'
import { PostTagsComponent } from '../PostTags/posttags.component'

describe('PostComponent', () => {
  let component: PostComponent
  let fixture: ComponentFixture<PostComponent>

  const mockPostData: Object = {
    '_id': '12345abc',
    'title': 'Foo',
    'content': 'Bar',
    'tags': [ 'foo', 'bar' ]
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostComponent,
        PostTagsComponent
      ],
      imports: [
        MomentModule,
        RouterTestingModule
      ]
    })

    TestBed.overrideComponent(PostTagsComponent, {
      set: { template: '<div>PostTagsComponent</div>' }
    })
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent)
    component = fixture.componentInstance
    component.post = mockPostData
    fixture.detectChanges()

  })

  it('should be a PostComponent', () => {
    expect(component instanceof PostComponent).toBeTruthy()
  })

  describe('onTitleClick', () => {
    beforeEach(() => {
      spyOn(component.openPost, 'emit')
      component.onTitleClick()
    })

    it('should emit openPost with the post id', () => {
      expect(component.openPost.emit).toHaveBeenCalledWith('12345abc')
    })
  })
})
