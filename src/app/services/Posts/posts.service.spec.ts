import { TestBed, inject } from '@angular/core/testing'
import { MockBackend, MockConnection } from '@angular/http/testing'
import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http'

import { PostsService } from './posts.service';

const mockHttpProvider = {
  deps: [ MockBackend, BaseRequestOptions ],
  useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
    return new Http(backend, defaultOptions)
  }
}

describe('PostsService', () => {
  let service: PostsService = null
  let backend: MockBackend = null

  const mockPosts: Array<Object> = [
    { 'foo': 'FOO' },
    { 'bar': 'BAR' },
    { 'baz': 'BAZ' }
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PostsService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, defaultOptions) => {
            return new Http(mockBackend, defaultOptions)
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })
  })

  beforeEach(inject([PostsService, MockBackend], (postsService, mockBackend) => {
    service = postsService
    backend = mockBackend
  }))

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  describe('getAllPosts', () => {
    it('should make a GET request to /api/posts', (done) => {
      backend.connections.subscribe((connection: MockConnection) => {
        expect(connection.request.url).toEqual('/api/posts')
        expect(connection.request.method).toEqual(RequestMethod.Get)

        const options = new ResponseOptions({ body: mockPosts })
        connection.mockRespond(new Response(options))
      })

      service.getAllPosts().subscribe(() => { done() })
    })
  })

  describe('getFilteredPosts', () => {
    it('should make a GET request to /api/posts/filter with the filter value', (done) => {
      backend.connections.subscribe((connection: MockConnection) => {
        expect(connection.request.url).toEqual('/api/posts/filter/foo')
        expect(connection.request.method).toEqual(RequestMethod.Get)

        const options = new ResponseOptions({ body: mockPosts })
        connection.mockRespond(new Response(options))
      })

      service.getFilteredPosts('foo').subscribe(() => { done() })
    })
  })

  describe('getPost', () => {
    it('should make a GET request to /api/posts/ with the post id value', (done) => {
      backend.connections.subscribe((connection: MockConnection) => {
        expect(connection.request.url).toEqual('/api/posts/12345abc')
        expect(connection.request.method).toEqual(RequestMethod.Get)

        const options = new ResponseOptions({ body: mockPosts })
        connection.mockRespond(new Response(options))
      })

      service.getPost('12345abc').subscribe(() => { done() })
    })
  })

  describe('createPost', () => {
    it('should make a POST request to /api/posts/ with the new post data', (done) => {
      backend.connections.subscribe((connection: MockConnection) => {
        expect(connection.request['url']).toEqual('/api/posts')
        expect(connection.request['method']).toEqual(RequestMethod.Post)
        expect(connection.request['_body']).toEqual({ 'foo': 'foo', 'bar': 'bar' })

        const options = new ResponseOptions({ body: mockPosts })
        connection.mockRespond(new Response(options))
      })

      service.createPost({ 'foo': 'foo', 'bar': 'bar' }).subscribe(() => { done() })
    })
  })
})
