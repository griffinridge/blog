import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/add/operator/map'

@Injectable()
export class PostsService {

  constructor(private http: Http) { }

  // Get all posts from the API
  getAllPosts() {
    return this.http.get('/api/posts')
      .map(res => res.json())
  }

  // Get all posts from the API
  getFilteredPosts(filter) {
    return this.http.get('/api/posts/filter/' + filter)
      .map(res => res.json())
  }

  // Get a single post from the API
  getPost(id) {
    return this.http.get('/api/posts/' + id)
      .map(res => res.json())
  }

  // Get a single post from the API
  createPost(newPost) {
    return this.http.post('/api/posts', newPost)
      .map(res => res.json())
  }
}
