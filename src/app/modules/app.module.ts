import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { MomentModule } from 'angular2-moment'
import { FocusModule } from 'angular2-focus'

// Services
import { PostsService } from '../services/Posts/posts.service'

// Components
import { AppComponent } from '../components/App/app.component'
import { CreatePostComponent } from '../components/CreatePost/createpost.component'
import { PostComponent } from '../components/Post/post.component'
import { PostTagsComponent } from '../components/PostTags/posttags.component'
import { PostsListComponent } from '../components/PostsList/postslist.component'

// Routes
const ROUTES = [
  { path: '', redirectTo: 'posts', pathMatch: 'full' },
  { path: 'posts', component: PostsListComponent },
  { path: 'posts/:id', component: PostsListComponent },
  { path: 'posts/filter/:filter', component: PostsListComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    CreatePostComponent,
    PostComponent,
    PostTagsComponent,
    PostsListComponent
  ],
  imports: [
    BrowserModule,
    FocusModule.forRoot(),
    FormsModule,
    HttpModule,
    MomentModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    PostsService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
