# Blog

The base project was created with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

## Dependencies

* [Node.js](https://nodejs.org/en/) version 6.10.3 (or latest v6.x LTS)
* [MongoDB](https://www.mongodb.com) version 3.4.2

## Recommended

* [angular-cli](https://github.com/angular/angular-cli) version 1.1.0

## Development

### Installing node packages

```bash
npm install
```

### Starting the Database

```bash
npm run db
```

### Application Quickstart

```bash
npm start
```

## View the site

Visit http://localhost:3000 to view the site.  If everything has gone to plan, you should be seeing this:

![No blog posts](screenshot.png)

## Running unit tests

To execute the unit tests
```bash
npm test
```

## Running JS Lint

To lint the code:
```bash
npm run lint
```

## Running end-to-end tests

To execute the end-to-end tests
```bash
npm e2e
```

Before running the tests make sure you are serving the app via `npm start`.

## TODO

* Mongoose: mpromise is deprecated - replace with another Promise library
* More unit tests
* Include test coverage
* Add e2e tests
* Improve text editor to support Markdown
* Add validation (required fields, max length etc)
* Production build script
* Edit/Delete blog posts
* Authentication (obviously)
